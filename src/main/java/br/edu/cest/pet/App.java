package br.edu.cest.pet;

import java.util.ArrayList;
import java.util.List;

import br.edu.cest.pet.entity.arca.AnimalAbs;
import br.edu.cest.pet.entity.arca.AnimalIFC;
import br.edu.cest.pet.entity.arca.Canino;
import br.edu.cest.pet.entity.arca.Felino;
import br.edu.cest.pet.entity.arca.Gato;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Bem vindo!");
		AnimalAbs a = new Canino("Rex");
		AnimalAbs b = new Canino("Rox");
		Felino c = new Felino("Alex");
		Felino g = new Gato("Tom");

		List<AnimalAbs> arca = new ArrayList<AnimalAbs>();
		arca.add(a);
		arca.add(b);
		arca.add(c);
		arca.add(g);
		
		for (AnimalIFC x: arca) {
			System.out.println("" + x.toString());
		}
		
		
	}
}
