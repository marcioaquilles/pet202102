package br.edu.cest.pet.entity;

import java.util.ArrayList;
import java.util.List;

import br.edu.cest.pet.divisao.Atendimento;
import br.edu.cest.pet.divisao.Clinica;
import br.edu.cest.pet.divisao.RH;
import br.edu.cest.pet.divisao.Setor;
import br.edu.cest.pet.entity.staff.Funcionario;

public class Empresa {

	// Funcionarios - Veterinarios / Atendentes
	// Clientes - contem os animais de estimacao

	private String nome;
	private String cnpj;
	private ArrayList<Setor> setores = new ArrayList<Setor>();

	// Lista de Funcionarios
	public List<Funcionario> listaFuncionarios = new ArrayList<Funcionario>();

	public Empresa(String nome, String cnpj) {
		super();
		this.nome = nome;
		this.cnpj = cnpj;
		setores.add(new Atendimento());
		setores.add(new RH());
		setores.add(new Clinica());
	}

	public void listarFuncionarios() {
		// TODO - for
		for (Setor s : setores) {
			System.out.println("Divisao:" + s.getDivisao());
			System.out.println("----------------------------");
			for (Funcionario f: s.getStaff()) {
				System.out.println("Funcionario:" + f.getNome());
			}
		}
	}

}
