package br.edu.cest.pet.entity.arca;

import br.edu.cest.pet.vo.EnumLocomocao;

public class Canino extends AnimalAbs {

	public Canino(String nome) {
		super(nome);
	}

	@Override
	public String getEspecie() {
		return "canino";			
	}
	
	@Override
	public String getFala() {
		return "late";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}
	

}
