package br.edu.cest.pet.entity.arca;

import java.math.BigDecimal;

public abstract class AnimalAbs implements AnimalIFC {
	private String nome;
	private BigDecimal peso;

	public AnimalAbs(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		//String b = "teste";
		//b = b.concat(" - complemento");
		//b = b + "complemento";String
		StringBuffer sb = new StringBuffer();
		sb.append("Nome:" + getNome());
		sb.append("\nFala:" + getFala());
		sb.append("\nPatas:" + getLocomocao());
		sb.append("\nEspecie:" + getEspecie());
		return sb.toString();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}	


}
