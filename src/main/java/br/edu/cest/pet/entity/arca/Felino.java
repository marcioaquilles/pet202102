package br.edu.cest.pet.entity.arca;

import br.edu.cest.pet.vo.EnumLocomocao;

public class Felino extends AnimalAbs {
	public Felino(String nome) {
		super(nome);
	}
	public void limpeza() {
		System.out.
		println("autolimpante");
	}
	public void subirNoTelhado() {
		System.out.println(
				"subiu no telhado");
	}
	@Override
	public String getFala() {
		return "mia";
	}
	@Override
	public String getEspecie() {
		return "felino";
	}
	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}
	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}
}
