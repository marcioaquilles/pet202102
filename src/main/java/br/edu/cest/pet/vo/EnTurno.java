package br.edu.cest.pet.vo;

public enum EnTurno {
	MAT(1), VESP(2), NOT(3);
	public int value;
	
	private EnTurno(int value) {
		this.value = value;
	}
	
}
